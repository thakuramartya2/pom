package org.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
//abc
public class GoogleHomepageObjects {
	//Initialize my object in the page
public GoogleHomepageObjects(WebDriver driver) {
	PageFactory.initElements(driver, this);
}	//abc//bde
	
	@FindBy(id="gs_htif0")
	public WebElement txtSearch;
	@FindBy(xpath="//button[@id ='mKlEF']")
	public WebElement btnSearch;
	@FindBy(linkText="Selenium - Web Browser Automation")
	public WebElement lnkSelenium;
	
	public void  SearchGoogle(String serchText )
	{
		txtSearch.sendKeys(serchText);
		//Click the search button
		btnSearch.click();
	}
	public SeleniumPageObjects clickSelenium()
	{
		lnkSelenium.click();
		return new SeleniumPageObjects ();
	//
		//
		
	}//
	//
	//
	
	
}
